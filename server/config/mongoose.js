/*
 Class: Mongoose
    Mongoose configuration file.
 Created by:
    kimduk on 16.08.15.
 */
var mongoose = require('mongoose'),
    userModel = require('../models/user'),
    connectionModel = require('../models/connection');

module.exports = function (config) {
    /*
     Function: connect
        Mongo DB connection
     Parameters:
        config.db = MongoDB name and other options
     */
    mongoose.connect(config.db);
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error...'));
    db.once('open', function callback() {
        console.log('network_service db opened');
    });

    userModel.createTestUsers();
    connectionModel.createTestConnections();
};
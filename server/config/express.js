/*
 Class: Express
 Express configuration file.
 Created by:
     kimduk on 16.08.15.
 */
var express = require('express'),
    stylus = require('stylus'),
    logger = require('morgan'),
    passport = require('passport'),
    session = require('express-session'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser');

module.exports = function (app, config) {
    /*
     Function: compile
        Set Stylus to compile CSS.
     Parameters:
         str - stylus str
         path - path to file
     */
    function compile(str, path) {
        return stylus(str).set('filename', path);
    }

    /*
     Function: set
        Set views files configs
     */
    app.set('views', config.rootPath + '/server/views');
    app.set('view engine', 'jade');

    app.use(cookieParser());
    app.use(session({secret: 'network service str'}));
    app.use(passport.initialize());
    app.use(passport.session());
    /*
     Function: use
        Additional dev modules
     */
    app.use(logger('dev'));
    app.use(bodyParser());

    app.use(stylus.middleware(
        {
            src: config.rootPath + '/public',
            compile: compile
        }
    ));

    app.use(express.static(config.rootPath + '/public'));
}
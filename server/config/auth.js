/*
 Class: Auth
 Auth configuration file.
 Created by:
    kimduk on 16.08.15.
 */
var passport = require('passport');
/*
 Function: authenticate
 Export authenticate method.
 Parameters:
    req - request variable
    res - response variable
    next - callback method
 */
exports.authenticate = function (req, res, next) {
    var auth = passport.authenticate('local', function (err, user) {
        if (err) {
            return next(err);
        }
        if (!user) {
            res.send({success: false});
        }
        req.logIn(user, function (err) {
            if (err) {
                return next(err);
            }
            res.send({success: true, user: user});
        });
    });
    auth(req, res, next);
};
/*
 Function: requiresApiLogin

 Parameters:
     req - request variable
     res - response variable
     next - callback method
 */
exports.requiresApiLogin = function(req, res, next) {
    if(!req.isAuthenticated()) {
        res.status(403);
        res.end();
    } else {
        next();
    }
};
/*
 Function: requiresRole
 Check is User role is correct
 Parameters:
     role - role of the user
 */
exports.requiresRole = function(role) {
    return function(req, res, next) {
        if(!req.isAuthenticated() || (req.user.roles.indexOf(role) === -1)) {
            res.status(403);
            res.end();
        } else {
            next();
        }
    }
}
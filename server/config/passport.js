/**
 * Created by kimduk on 16.08.15.
 */

var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    mongoose = require('mongoose'),
    User = mongoose.model('User');

module.exports = function () {

    passport.use(new LocalStrategy({
            usernameField: 'uid',
            passwordField: 'password'
        },
        function(uid, password, done) {
            User.findOne({uid: uid}).exec(function(err, user) {
                if (user && user.authenticate(password)) {
                    return done(null, user);
                } else {
                    return done(null, false);
                }
            });
        }
    ));

    passport.serializeUser(function(user, done) {
        if (user) {
            done(null, user._id);
        }
    });

    passport.deserializeUser(function(id, done) {
        User.findOne({_id: id}).exec(function(err, user) {
            if (user) {
                return done(null, user);
            } else {
                return  done(null, false);
            }
        });
    });
}
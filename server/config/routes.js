/*
 Class: Routes
    All project Routes file.
 Created by:
    kimduk on 16.08.15.
 */
var auth = require('./auth'),
    mongoose = require('mongoose'),
    users = require('../controllers/users'),
    connection = require('../controllers/connection'),
    User = mongoose.model('User');

module.exports = function(app) {

    app.get('/api/friends/:uid', auth.requiresRole('user'), connection.getUserFriends);
    app.post('/api/friends/', auth.requiresRole('user'), connection.saveFriendship);
    app.delete('/api/friends/:id', auth.requiresRole('user'), connection.removeFriendship);

    app.get('/api/users', auth.requiresRole('admin'), users.getUsers);
    app.get('/api/users/:uid', auth.requiresRole('user'), users.getUserByUid);
    app.post('/api/users', auth.requiresRole('admin'), users.createUser);
    /**
     * Route for Angular partials views
     */
    app.get('/partials/*', function(req, res) {
        res.render('../../public/js/app/' + req.params[0]);
    });

    /**
     * Auth routes
     */
    app.post('/login', auth.authenticate);
    app.post('/logout', function(req, res) {
        req.logout();
        res.end();
    });

    app.get('*', function(req, res) {
        console.log(req.user);
        res.render('index', {
            bootstrappedUser: req.user
        });
    });
}
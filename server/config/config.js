/*
 Class: Config
 Configuration file.
 Created by:
    kimduk on 16.08.15.
 */
var path = require('path'),
    rootPath = path.normalize(__dirname + '/../../');

module.exports = {
    development: {
        db: 'mongodb://localhost/network_service',
        port: process.env.PORT || 3030,
        rootPath: rootPath
    },
    production: {
        db: 'mongodb://kimduk:kimduk@ds033103.mongolab.com:33103/network_service',
        port: process.env.PORT || 80,
        rootPath: rootPath
    }
}
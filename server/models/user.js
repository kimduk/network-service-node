/*
 Class: User Model
 A class that manages an users data.
 Created by:
    kimduk on 16.08.15.
 */
var mongoose = require('mongoose'),
    encrypt = require('../utilities/encrypt');

/**
 * User Model schema
 * @type {*}
 */
var userSchema = mongoose.Schema({
    uid: { type: Number, min: 1, max: 10000000, unique: true },
    password: {type: String, required: '{PATH} is required'},
    avatar: String,
    salt: String,
    roles: [String],
    deleted: { type: Number, min: 0, max: 1 }
});
/*
 Function: authenticate
 Authenticate User if password correct.
 Parameters:
    password - response param
 */
userSchema.methods = {
    authenticate: function(password) {
        return encrypt.hashPwd(this.salt, password) === this.password;
    }
}
var User = mongoose.model('User', userSchema);

/*
 Function: createTestUsers
 Create Test Users data.
 */
function createTestUsers() {
    User.find({}).exec(function(err, collection) {
        if (collection.length === 0) {
            var salt, hash;

            salt = encrypt.createSalt();
            hash = encrypt.hashPwd(salt, 'password');
            User.create({uid: 1, password: hash, salt: salt, avatar: 'https://s.gravatar.com/avatar/9ae028f24d7f1f92fd3a0e9d473acf13?s=80', roles: ['user', 'admin'], deleted: 0
            });

            salt = encrypt.createSalt();
            hash = encrypt.hashPwd(salt, 'password');
            User.create({uid: 3154, password: hash, salt: salt, avatar: '', deleted: 0, roles: ['user']});

            salt = encrypt.createSalt();
            hash = encrypt.hashPwd(salt, 'password');
            User.create({uid: 154, password: hash, salt: salt, avatar: 'https://s.gravatar.com/avatar/9ae028f24d7f1f92fd3a0e9d473acf13?s=80', deleted: 0, roles: ['user']});

            salt = encrypt.createSalt();
            hash = encrypt.hashPwd(salt, 'password');
            User.create({uid: 1454, password: hash, salt: salt, avatar: '', deleted: 0, roles: ['user']});
        }
    });
}

exports.createTestUsers = createTestUsers;
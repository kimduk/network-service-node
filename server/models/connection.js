
/*
 Class: Connection
 Connection model file.
 Created by:
    kimduk on 17.08.15.
 */
var mongoose = require('mongoose');

/**
 * Connection Model Schema
 * @type {*}
 */
var connectionSchema = mongoose.Schema({
    inviterId: {type: Number, required: '{PATH} is required!'},
    invitedId: {type: Number, required: '{PATH} is required!'},
    inviterConfirm: Boolean,
    invitedConfirm: Boolean
});

var Connection = mongoose.model('Connection', connectionSchema);
/*
 Function: createTestConnections
 Create Test Connections data.
 */
function createTestConnections() {
    Connection.find({}).exec(function(err, collection) {
        if (collection.length === 0) {
            Connection.create({inviterId: 1, invitedId: 3154, inviterConfirm: true, invitedConfirm: true});
            Connection.create({inviterId: 1, invitedId: 154, inviterConfirm: true, invitedConfirm: true});
            Connection.create({inviterId: 1, invitedId: 1454, inviterConfirm: true, invitedConfirm: false});
            Connection.create({inviterId: 1, invitedId: 2, inviterConfirm: true, invitedConfirm: true});

        }
    })
}

exports.createTestConnections = createTestConnections;
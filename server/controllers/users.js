/*
 Class: User
    User Controller file.
 Created by:
    kimduk on 16.08.15.
 */
var User = require('mongoose').model('User')
    encrypt = require('../utilities/encrypt');

/*
 Function: getUsers
 Get All Users List.
 Parameters:
    req - request variable
    res - response variable
*/
exports.getUsers = function (req, res) {
    User.find({}).exec(function (err, collection) {
        res.send(collection);
    });
};

/*
 Function: getUserByUid
 Get User Item by UID.
 Parameters:
     req - request variable
     res - response variable
 */
exports.getUserByUid = function (req, res) {
    User.findOne({"uid": req.params.uid}).exec(function (err, collection) {
        if (err) {
            res.send(err);
        } else {
            res.send(collection);
        }
    });
};

/*
 Function: createUser
 Create new User.
 Parameters:
     req - request variable
     res - response variable
    next - callback method
 */
exports.createUser = function (req, res, next) {
    var userData = req.body;
    userData.salt = encrypt.createSalt();
    userData.password = encrypt.hashPwd(userData.salt, userData.password);
    userData.roles = ["user"];

    User.create(userData, function(err, user) {
        if (err) {
            if(err.toString().indexOf('E11000') > -1) {
                err = new Error('Duplicate Uid');
            }
            res.status(400);
            return res.send({reason: err.toString()});
        }
        res.send(user);
    });
};
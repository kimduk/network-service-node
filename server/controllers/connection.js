/*
 Class: Connection
    Connection Controller file.
 Created by:
    kimduk on 17.08.15.
 */
var Connection = require('mongoose').model('Connection'),
    User = require('mongoose').model('User'),
    async = require('async');

/*
 Function: getUserFriends
    Get user friends by uid with async
 Parameters:
     req - request variable
     res - response variable
 */
exports.getUserFriends = function (req, res) {
    Connection.find({
            "$or": [{
                "inviterId": req.params.uid
            }, {
                "invitedId": req.params.uid
            }]
        })
        .lean()
        .exec(function(err, connection) {
        async.map(connection, function(key, next) {
            User.find({uid: key.invitedId}).exec(function(err, user) {
                key.invitedInfo = user;
                next(err, key);
            });

        }, function (err, result) {
            res.send(result);
        });
    });
}

/*
 Function: saveFriendship
     Create new friendship connection
 Parameters:
     req - request variable
     res - response variable
 */
exports.saveFriendship = function (req, res, next) {
    var friendshipData = req.body;
    Connection.find({
        "$or": [{
            "inviterId": friendshipData.uid, "invitedId": friendshipData.fuid
        }, {
            "inviterId": friendshipData.fuid, "invitedId": friendshipData.uid
        }]
    })
        .lean()
        .exec(function(err, connection) {
            if (connection.length > 0) {
                res.send({
                    success: false,
                    reason: 'Users already friends.'
                });
            } else {
                var newData = {
                    inviterId: friendshipData.uid,
                    invitedId: friendshipData.fuid,
                    inviterConfirm: true,
                    invitedConfirm: false
                };
                Connection.create(newData, function(err, user) {
                    if (err) {
                        res.status(400);
                        return res.send({reason: err.toString()});
                    }
                    res.send({
                        success: true
                    });
                });
            }
        });
}
/*
 Function: removeFriendship
    Remove friendship connection between users
 Parameters:
    req - request variable
    res - response variable
    next - callback method
 */
exports.removeFriendship = function (req, res, next) {
    var friendshipId = req.params.id;
    Connection.find({_id: friendshipId})
        .remove()
        .exec(function(err, result) {
            if (err) {
                res.status(400);
                res.send({reason: err.toString()});
            }
            res.send({success: true});
        });
}
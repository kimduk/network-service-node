/*
 Class: Encrypt
 Crypto file used for Auth functions.
 Created by:
     kimduk on 16.08.15.
 */
var crypto = require('crypto');

/*
 Function: createSalt
 Generated  random Salt for user password.
 */
exports.createSalt = function () {
    return crypto.randomBytes(128).toString('base64');
}
/*
 Function: hashPwd
 Hashed User password.
 Parameters:
    salt - random string
    pwd - password String
 */
exports.hashPwd = function (salt, pwd) {
    var hmac = crypto.createHmac('sha1', salt);
    return hmac.update(pwd).digest('hex');
}
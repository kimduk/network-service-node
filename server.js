/**
 * Created by kimduk on 14.08.15.
 */

var express = require('express'),
    env = process.env.NODE_ENV = process.env.NODE_ENV || 'development',
    app = express(),
    config = require('./server/config/config')[env];
/**
 * Append Express configs
 */
require('./server/config/express')(app, config);
/**
 * Append MongoDB configs
 */
require('./server/config/mongoose')(config);
/**
 * Append Routes rules
 */
require('./server/config/routes')(app);
/**
 * Passport configs
 */
require('./server/config/passport')();



/**
 * Start application
 */
app.listen(config.port);

console.log('Port ' + config.port);

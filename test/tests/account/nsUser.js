/**
 * Created by kimduk on 18.08.15.
 */
describe('nsUser', function() {
    beforeEach(module('app'));

    describe('isAdmin', function() {
        it('should return false if admin is not in array of roles', inject(function(nsUser) {
            var user = new nsUser();
            user.roles = ['not admin rule'];
            expect(user.isAdmin()).to.be.falsey;
        }));

        it('should return true if admin is in array of roles', inject(function(nsUser) {
            var user = new nsUser();
            user.roles = ['admin'];
            expect(user.isAdmin()).to.be.true;
        }));

    });
});
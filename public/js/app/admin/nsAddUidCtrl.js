/**
 * Created by kimduk on 16.08.15.
 */

angular.module('app').controller('nsAddUidCtrl', function ($scope, nsAuth, $location, nsNotifier) {
    /**
     * Add new User in system
     */
    $scope.addUid = function () {
        var newUserData = {
            uid: $scope.newUid,
            password: $scope.password,
            avatar: $scope.avatar
        };

        nsAuth.createUid(newUserData).then(function () {
            nsNotifier.notify('Uid is created');
            $location.path('/admin/users');
        }, function(reason) {
            nsNotifier.error(reason);
        });
    };
    /**
     * Auto-generation of UId integer number
     */
    $scope.generateUid = function () {
        $scope.newUid = getRandomInt(1, 1000000);
    };

    $scope.generatePwd = function () {
        $scope.password = getRandomString(6);
    };

    function getRandomInt(min, max) {
        return parseInt(Math.random() * (max - min) + min);
    };

    function getRandomString(count) {
        var result = "",
            possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for( var i=0; i < count; i++ ) {
            result += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return result;
    };
});
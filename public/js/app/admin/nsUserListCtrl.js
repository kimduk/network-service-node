/**
 * Created by kimduk on 16.08.15.
 */

angular.module('app').controller('nsUserListCtrl', function($scope, nsUser) {
    $scope.users = nsUser.query();
});
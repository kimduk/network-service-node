/**
 * Created by kimduk on 17.08.15.
 */

angular.module('app').factory('nsFriends', function ($resource) {
    var FriendsResource = $resource('/api/friends/:id', {}, {
        query: {method:'GET', params:{id:'id'}, isArray:true}
    });

    return FriendsResource;
});
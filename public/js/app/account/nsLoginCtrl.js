/**
 * Created by kimduk on 16.08.15.
 */

angular.module('app').controller('nsLoginCtrl', function ($scope, $http, nsIdentity, nsNotifier, nsAuth, $location) {
    $scope.identity = nsIdentity;
    /**
     * Sign In (Login)
     * @param uid
     * @param password
     */
    $scope.signin = function (uid, password) {
        nsAuth.authenticateUser(uid, password).then(function (success) {
            if (success) {
                nsNotifier.notify('You are successfully login, <br/> Welcome to NetworkService');
                $location.path('/profile');
            } else {
                nsNotifier.error('Wrong Login or Password, <br/> Please, try again.')
            }
        });
    }

    /**
     * Logout from system
     */
    $scope.signout = function() {
        nsAuth.logoutUser().then(function () {
            $scope.uid = undefined;
            $scope.password = undefined;
            nsNotifier.notify('You are successfully logout, <br/> Comeback soon.');
            $location.path('/');
        });
    }
});
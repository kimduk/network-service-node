/**
 * Created by kimduk on 16.08.15.
 */

angular.module('app').factory('nsUser', function ($resource) {
    var UserResource = $resource('/api/users/:uid', {}, {
        update: {method: 'PUT', isArray: false},
        query: {method:'GET', isArray:true},
        get: {method:'GET', isArray:false}
    });

    UserResource.prototype.isAdmin = function () {
        return this.roles && this.roles.indexOf('admin') > -1;
    }

    return UserResource;
});
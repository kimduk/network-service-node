/**
 * Created by kimduk on 16.08.15.
 */

angular.module('app').factory('nsAuth', function($http, nsIdentity, $q, nsUser) {
    return {
        /**
         * User Authenticate
         * @param uid
         * @param password
         * @returns {context.promise|*|promise|promiseAndHandler.promise|defer.promise|Promise.deferred.promise}
         */
        authenticateUser: function(uid, password) {
            var def = $q.defer();
            $http.post('/login', {uid: uid, password: password}).then(function(response) {
                if (response.data.success) {
                    var user = new nsUser();
                    angular.extend(user, response.data.user);
                    nsIdentity.currentUser = user;
                    def.resolve(true);
                } else {
                    def.resolve(false);
                }
            });

            return def.promise;
        },
        /**
         * Logout user from system
         * @returns {context.promise|*|promise|promiseAndHandler.promise|defer.promise|Promise.deferred.promise}
         */
        logoutUser: function() {
            var def = $q.defer();
            $http.post('/logout', {logout: true}).then(function() {
                nsIdentity.currentUser = undefined;
                def.resolve();
            });

            return def.promise;
        },
        authorizeCurrentUserForRoute: function (role) {
            if (nsIdentity.isAuthorized(role)) {
                return true;
            } else {
                return $q.reject('not authorized');
            }
        },
        authorizeAuthenticatedUserForRoute: function () {
            if (nsIdentity.isAuthenticated()) {
                return true;
            } else {
                return $q.reject('not authorized');
            }
        },
        /**
         * Create new User
         * @param newUserData
         * @returns {context.promise|*|promise|promiseAndHandler.promise|defer.promise|Promise.deferred.promise}
         */
        createUid: function(newUserData) {
            var newUser = new nsUser(newUserData);
            var dfd = $q.defer();

            newUser.$save().then(function () {
                dfd.resolve();
            }, function (response) {
                dfd.reject(response.data.reason);
            });

            return dfd.promise;
        }
    }
});
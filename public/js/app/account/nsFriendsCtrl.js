/**
 * Created by kimduk on 17.08.15.
 */

angular.module('app').controller('nsFriendsCtrl', function ($scope, nsFriends, nsIdentity, nsNotifier, nsUser, $location) {
    $scope.friends = nsFriends.query({id: nsIdentity.currentUser.uid});
    /**
     * Search friend by uid
     * @type {boolean}
     */
    $scope.showResult = false;
    $scope.searchUid = function () {
        nsUser.get({uid: $scope.findUid})
            .$promise.then(function(result) {
                if (result.uid !== undefined) {
                    nsNotifier.notify('Uid is find.')
                    $scope.result = result;
                    $scope.showResult = true;
                } else {
                    nsNotifier.error('Uid is not find.');
                    $scope.result = [];
                    $scope.showResult = false;
                }
            });
    };

    /**
     * Create new Friendship connection
     */
    $scope.addFriend = function () {
        var newData = {fuid: $scope.findUid, uid: nsIdentity.currentUser.uid};
        var newFriendship = new nsFriends(newData);

        newFriendship.$save().then(function (response) {
            if (response.success) {
                nsNotifier.notify('Connection is ready!');
                $location.path('/friends');
            } else {
                nsNotifier.error(response.reason);
            }
        }, function (response) {
            nsNotifier.error(response.data.reason);
        });
    };

    $scope.removeFriend = function (connectionId) {
        var friendship = new nsFriends(connectionId);

        friendship.$delete({id: connectionId}).then(function (response) {
            if (response.success) {
                nsNotifier.notify('Friend is remove!');
                $scope.friends = nsFriends.query({id: nsIdentity.currentUser.uid});
            } else {
                nsNotifier.error(response.reason);
            }
        }, function (response) {
            nsNotifier.error(response.data.reason);
        });
    }
});

/**
 * Created by kimduk on 15.08.15.
 */

angular.module('app', ['ngResource', 'ngRoute']);

angular.module('app').config(function($routeProvider, $locationProvider) {
    var routeRoleChecks = {
        admin: {auth: function(nsAuth) {
            return nsAuth.authorizeCurrentUserForRoute('admin')
        }},
        user: {auth: function(nsAuth) {
            return nsAuth.authorizeAuthenticatedUserForRoute()
        }}
    }
    $routeProvider
        .when('/', { templateUrl: '/partials/main/main', controller: 'nsMainCtrl'})
        .when('/admin/users', { templateUrl: '/partials/admin/user-list', controller: 'nsUserListCtrl', resolve: routeRoleChecks.admin})
        .when('/admin/add-uid', { templateUrl: '/partials/admin/add-uid', controller: 'nsAddUidCtrl', resolve: routeRoleChecks.admin})
        .when('/profile', { templateUrl: '/partials/account/profile', controller: 'nsProfileCtrl', resolve: routeRoleChecks.user})
        .when('/friends', { templateUrl: '/partials/account/friends', controller: 'nsFriendsCtrl', resolve: routeRoleChecks.user})
        .when('/friends-search', { templateUrl: '/partials/account/search', controller: 'nsFriendsCtrl', resolve: routeRoleChecks.user});

    $locationProvider.html5Mode(true);
});

angular.module('app').run(function($rootScope, $location) {
    $rootScope.$on('$routeChangeError', function(evt, current, previous, rejection) {
        if(rejection === 'not authorized') {
            $location.path('/');
        }
    })
});
/**
 * Created by kimduk on 16.08.15.
 */

angular.module('app').factory('nsIdentity', function ($window, nsUser) {
    var currentUser = undefined;
    if (!!$window.bootstrappedUserObj) {
        currentUser = new nsUser();
        angular.extend(currentUser, $window.bootstrappedUserObj);
    }
    return {
        currentUser: currentUser,
        isAuthenticated: function() {
            return !!this.currentUser;
        },
        isAuthorized: function (role) {
            return !!this.currentUser && this.currentUser.roles.indexOf(role) > -1;
        }
    }
});
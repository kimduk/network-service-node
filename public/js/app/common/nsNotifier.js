/**
 * Created by kimduk on 16.08.15.
 */

angular.module('app').value('nsToastr', toastr);

angular.module('app').factory('nsNotifier', function (nsToastr) {
    return {
        notify: function (msg) {
            nsToastr.success(msg);
        },
        error: function (msg) {
            nsToastr.error(msg);
        }
    }
});